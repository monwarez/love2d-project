function	loadMap(tileset)
	Tileset	=	love.graphics.newImage(tileset)

	local tileW,tileH	=	32,32
	local	tilesetW,tilesetH	=	Tileset:getWidth(), Tileset:getHeight()

	GrassQuad2	=	love.graphics.newQuad(0,0,tileW,tileH,tilesetW,tilesetH)
	GrassQuad1	=	love.graphics.newQuad(32,0,tileW,tileH,tilesetW,tilesetH)

end
function	drawMap()
	local baseX	=	328
	local baseY = 268
	local tileW,tileH	=	32,32
	-- first layer
	for i=0,30 do
		for j=0,30 do
			love.graphics.draw(Tileset, GrassQuad1, i*tileW, j*tileH)
		end
	end
	-- second layer
	love.graphics.draw(Tileset, GrassQuad2, baseX + tileW, baseY + tileH)
end
