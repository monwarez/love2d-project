function loadChar(chartile)
  CharTile = love.graphics.newImage(chartile) 
  local  tileW,tileH  =   32,64
  local  charW,charH  =  CharTile:getWidth(), CharTile:getHeight()
  CharWalk  = {}
  for i=0,3 do
    for j=0,3 do
      CharWalk[i*4 +j] = love.graphics.newQuad(i*tileW, j*tileH, tileW, tileH, charW, charH)
    end
  end
  CharDirection = "down"
  IsIdle = 1
end

function drawChar(x,y, tileW, tileH, elapsed)
  if (IsIdle == 1) then
    if CharDirection == "down" then
      love.graphics.draw(CharTile, CharWalk[0], x*tileW, y*tileH)
    end
    if CharDirection == "up" then
      love.graphics.draw(CharTile, CharWalk[4], x*tileW, y*tileH)
    end
    if CharDirection == "left" then
      love.graphics.draw(CharTile, CharWalk[8], x*tileW, y*tileH)
    end
    if CharDirection == "right" then
      love.graphics.draw(CharTile, CharWalk[12], x*tileW, y*tileH)
    end
  end
  if (IsIdle == 0) then
    local j = math.floor(elapsed*1000) % 1000
    if j >= 750 then
      j = 3
    end
    if j >= 500 then
      j = 2
    end
    if j >= 250 then
      j = 1
    end
    if j>= 4 then
      j = 0
    end

    if CharDirection == "down" then
      love.graphics.draw(CharTile, CharWalk[0+j], x*tileW, y*tileH)
    end
    if CharDirection == "up" then
      love.graphics.draw(CharTile, CharWalk[4+j], x*tileW, y*tileH)
    end
    if CharDirection == "left" then
      love.graphics.draw(CharTile, CharWalk[8+j], x*tileW, y*tileH)
    end
    if CharDirection == "right" then
      love.graphics.draw(CharTile, CharWalk[12+j], x*tileW, y*tileH)
    end
  end
end
function charDirection(direction)
  CharDirection = direction
end
function charSetIdle(idle_state)
  IsIdle        = idle_state
end
