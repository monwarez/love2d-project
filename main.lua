require 'map-functions.lua'
require 'char-functions.lua'
function love.load()
  StartTime = love.timer.getTime()
  love.keyboard.setKeyRepeat(enable)
  loadMap("res/green_country.png")
  loadChar("res/char_normal.png")
  local _, _, flags 	=	love.window.getMode()
  window		=	{}
  window.width, window.height	=	love.graphics.getDimensions(flags.display)
  hero 		=	{}
  hero.x 		=  10
  hero.y		=  10
  hero.speed	=  2
  quitA,quitB,quitC,quitD = window.width - 50, window.height - 30, window.width -1, window.height -1
  quitX,quitY	=	window.width - 40,window.height - 20
  quitRectTab	=	{
    quitA, quitB,
    quitC, quitB,
    quitC, quitD,
    quitA, quitD,
    quitA, quitB}
end
function love.draw()
  local tileW,tileH = 32,32
  love.graphics.clear()
  love.graphics.setColor(255,255,255)
  -- draw map
  drawMap()
  local	step = 5 -- how much pixel for walk
  love.graphics.print("Quit", quitX, quitY)
  love.graphics.setColor(0,0,255)
  love.graphics.line(quitRectTab)
  love.graphics.setColor(255,255,200)
  love.graphics.setColor(255,0,0)
  drawChar(hero.x, hero.y, tileW,tileH, love.timer.getTime() - StartTime)
  -- love.graphics.print("@", hero.x, hero.y)
end
function isAABBCollision(x,y,a,b,c,d)
-- Better do AABB bounding box collision
  if x >= a then
    if x <= c then
      if y >= b then
        if y <= d	then
          return true
        end
      end
    end
  end
return false
end
function love.touchpressed(id,x,y,dx,dy,pressure)
  -- Allow the app to quit when there is no keyboard
  if isAABBCollision(x,y,quitA,quitB,quitC,quitD)	then
    love.event.quit()
  end
end
function love.mousepressed(x,y,button, istouch)
  -- Allow the app to quit when there is no keyboard
  if button == 1 then
    if isAABBCollision(x,y,quitA,quitB,quitC,quitD)	then
      love.event.quit()
    end
  end
end
function correctBorder(entity)
	-- change it later maybe
	local tileW,tileH	=	32,32
	if entity.x < 0 then
		entity.x	=	0
	end
	if entity.x > window.width - tileW then
		entity.x	=	window.width - tileW
	end
	if entity.y < 0 then
		entity.y	=	0
	end
	if entity.y > window.height - tileH then
		entity.y	=	window.height - tileH
	end
end	
function love.update(dt)
  local isIdle = 1
  -- Movment action
  if love.keyboard.isDown("up") then
    charDirection("up")
    isIdle = 0
    hero.y	=	hero.y - hero.speed*dt
  end
  if love.keyboard.isDown("down") then
    charDirection("down")
    isIdle = 0
    hero.y	=	hero.y + hero.speed*dt
  end
  if love.keyboard.isDown("left") then
    charDirection("left")
    isIdle = 0
    hero.x	=	hero.x - hero.speed*dt
  end
  if love.keyboard.isDown("right") then
    charDirection("right")
    isIdle = 0
    hero.x	=	hero.x + hero.speed*dt
  end
  charSetIdle(isIdle)

  correctBorder(hero)
  -- the magic quit key
  if love.keyboard.isDown("escape") then
    love.event.quit()
  end
  if love.keyboard.isDown("q") then
    love.event.quit()
  end
end
